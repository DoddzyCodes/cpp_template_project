cmake_minimum_required(VERSION 3.0.0)
project(${PROJECT_FULL_NAME}_progam VERSION 0.0.0)

set(HEADERS
)

set(SRC
    main.cpp
)

add_executable(${PROJECT_NAME} ${HEADERS} ${SRC})
