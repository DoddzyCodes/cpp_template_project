#!/bin/bash

mkdir $1
template_dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
working_dir=$(pwd)

cd $template_dir

git archive master | tar -x -C $working_dir/$1

cd $working_dir/$1
rm clone.sh